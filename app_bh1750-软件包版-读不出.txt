#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include <string.h>
#include "sensor.h"

/* 定义一个BH1750线程句柄结构体指针 */
static rt_thread_t app_bh1750_thread = RT_NULL;

/* 发布"BH1750_Pub"主题消息函数 */
//void BH1750_Publish(char * msg);
char data_buf[32];

/* 消息队列控制块 */
struct rt_messagequeue light_mq;
/* 消息队列中用到的放置消息的内存池 */
static char light_msg_pool[2048];

static void app_bh1750_thread_entry(void *parameter)
{
    rt_device_t dev = RT_NULL;
    struct rt_sensor_data data;
    rt_size_t res;
    int result;

    /* 查找系统中的传感器设备 */
    dev = rt_device_find("li_bh1750");
    if (dev == RT_NULL)
    {
        rt_kprintf("Can't find device:li_bh1750\n");
        return;
    }

    /* 以轮询模式打开传感器设备 */
    if (rt_device_open(dev, RT_DEVICE_FLAG_RDONLY) != RT_EOK)
    {
        rt_kprintf("open device failed!");
        return;
    }

    while(1)
    {
        /* 从传感器读取一个数据 */
        res = rt_device_read(dev, 0, &data, 1);
        if (res != 1)
        {
            rt_kprintf("read data failed!size is %d", res);
        }
        else
        {
            rt_kprintf("light:%4d.%d lux \n", (&data)->data.light / 10, (&data)->data.light % 10);
            rt_sprintf(data_buf,"%d.%d", (&data)->data.light / 10, (&data)->data.light % 10);
            //BH1750_Publish(data_buf);
            /* 发送消息到消息队列中 */
            result = rt_mq_send(&light_mq, data_buf, sizeof(data_buf));
            rt_kprintf("bh1750 send msg: %s\n", data_buf);
            if ( result == -RT_EFULL)
            {
                   /* 消息队列满， 延迟1s时间 */
                   rt_kprintf("message queue full, delay 1s\n");
                   rt_thread_delay(1000);
             }
        }
        rt_thread_mdelay(60000);
    }
    /* 关闭传感器设备 */
    rt_device_close(dev);
}

static int app_bh1750_init(void)
{
    rt_err_t rt_err;

    /* 初始化消息队列 */
    rt_mq_init(&light_mq, "light_mqt",
               &light_msg_pool[0], /* 内存池指向msg_pool */
               128 - sizeof(void*), /* 每个消息的大小是 128 - void* */
               sizeof(light_msg_pool),  /* 内存池的大小是msg_pool的大小 */
               RT_IPC_FLAG_FIFO); /* 如果有多个线程等待，按照FIFO的方法分配消息 */

    /* 创建BH1750线程*/
    app_bh1750_thread = rt_thread_create("app_bh1750 thread",
            app_bh1750_thread_entry, RT_NULL, 2048, 6, 10);
    /* 如果获得线程控制块，启动这个线程 */
    if (app_bh1750_thread != RT_NULL)
        rt_err = rt_thread_startup(app_bh1750_thread);
    else
        rt_kprintf("app_bh1750_thread create failure !!! \n");

    /* 判断线程是否启动成功 */
    if( rt_err == RT_EOK)
        rt_kprintf("app_bh1750_thread startup ok. \n");
    else
        rt_kprintf("app_bh1750_thread startup err. \n");

    return rt_err;
}

INIT_APP_EXPORT(app_bh1750_init);
