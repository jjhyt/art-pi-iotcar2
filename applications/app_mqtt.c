#include <stdio.h>
#include <stdint.h>
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include "mqttclient.h"
#include "cJSON.h"

#define MQTT_HOST    "192.168.123.200"             // MQTT服务器的地址
#define MQTT_PORT    "1883"                        // MQTT服务器的端口号
#define MQTT_CLIENT    "123"                            // ClientId随意
#define MQTT_USERNAME    "admin"                   // 用户名
#define MQTT_PASSWORD    "123456"                  // 用户名对应的密码
#define MQTT_SUBTOPIC       "domoticz/out"         //订阅主题
#define MQTT_PUBTOPIC       "domoticz/in"          //推送主题
#define MOTO_2_1_PIN GET_PIN(B, 12)
#define MOTO_2_2_PIN GET_PIN(B, 13)
#define MOTO_3_1_PIN GET_PIN(B, 0)
#define MOTO_3_2_PIN GET_PIN(B, 1)
#define CAR_RED_PIN GET_PIN(C, 3)
#define CAR_GREEN_PIN GET_PIN(A, 1)
#define CAR_BLUE_PIN GET_PIN(A, 2)
#define CAR_LEFT_LED GET_PIN(G, 13)
#define CAR_RIGHT_LED GET_PIN(G, 14)

extern struct rt_messagequeue mq;
extern struct rt_messagequeue light_mq;
/* 定义一个MQTT线程句柄结构体指针 */
static rt_thread_t mqtt_thread = RT_NULL;
static mqtt_client_t *client = NULL;
/* PIR 触发信号量的指针 */
extern rt_sem_t pir_on_sem;
/* PIR 停止信号量的指针 */
extern rt_sem_t pir_off_sem;

/* 定义两个MQTT线程句柄结构体指针 */
static rt_thread_t mqtt_pir_on = RT_NULL;
static rt_thread_t mqtt_pir_off = RT_NULL;

void car_red_led(void)  //红灯
{
    rt_pin_write(CAR_RED_PIN, PIN_HIGH);
    rt_pin_write(CAR_GREEN_PIN, PIN_LOW);
    rt_pin_write(CAR_BLUE_PIN, PIN_LOW);
}

void car_green_led(void)  //绿灯
{
    rt_pin_write(CAR_RED_PIN, PIN_LOW);
    rt_pin_write(CAR_GREEN_PIN, PIN_HIGH);
    rt_pin_write(CAR_BLUE_PIN, PIN_LOW);
}

void car_blue_led(void)    //蓝灯
{
    rt_pin_write(CAR_RED_PIN, PIN_LOW);
    rt_pin_write(CAR_GREEN_PIN, PIN_LOW);
    rt_pin_write(CAR_BLUE_PIN, PIN_HIGH);
}

void car_led_off(void)     //关灯
{
    rt_pin_write(CAR_RED_PIN, PIN_LOW);
    rt_pin_write(CAR_GREEN_PIN, PIN_LOW);
    rt_pin_write(CAR_BLUE_PIN, PIN_LOW);
}

void car_led_on(void)
{
    rt_pin_write(CAR_RED_PIN, PIN_HIGH);
    rt_pin_write(CAR_GREEN_PIN, PIN_HIGH);
    rt_pin_write(CAR_BLUE_PIN, PIN_HIGH);
}

void car_go(void)  //前进
{
    rt_pin_write(MOTO_3_1_PIN, PIN_HIGH);
    rt_pin_write(MOTO_3_2_PIN, PIN_LOW);
    rt_pin_write(MOTO_2_1_PIN, PIN_HIGH);
    rt_pin_write(MOTO_2_2_PIN, PIN_LOW);
}

void car_back(void)  //后退
{
    rt_pin_write(MOTO_3_1_PIN, PIN_LOW);
    rt_pin_write(MOTO_3_2_PIN, PIN_HIGH);
    rt_pin_write(MOTO_2_1_PIN, PIN_LOW);
    rt_pin_write(MOTO_2_2_PIN, PIN_HIGH);
}

void car_left(void)  //左转
{
    rt_pin_write(MOTO_3_1_PIN, PIN_LOW);
    rt_pin_write(MOTO_3_2_PIN, PIN_HIGH);
    rt_pin_write(MOTO_2_1_PIN, PIN_LOW);
    rt_pin_write(MOTO_2_2_PIN, PIN_LOW);
}

void car_right(void)  //右转
{
    rt_pin_write(MOTO_3_1_PIN, PIN_LOW);
    rt_pin_write(MOTO_3_2_PIN, PIN_LOW);
    rt_pin_write(MOTO_2_1_PIN, PIN_LOW);
    rt_pin_write(MOTO_2_2_PIN, PIN_HIGH);
}

void car_stop(void)  //停止
{
    rt_pin_write(MOTO_3_1_PIN, PIN_LOW);
    rt_pin_write(MOTO_3_2_PIN, PIN_LOW);
    rt_pin_write(MOTO_2_1_PIN, PIN_LOW);
    rt_pin_write(MOTO_2_2_PIN, PIN_LOW);
}

static void sub_topic_handle1(void* client, message_data_t* msg)
{
    cJSON* json;
    (void) client;
    KAWAII_MQTT_LOG_I("----------------------");
    KAWAII_MQTT_LOG_I("%s:%d %s()...\ntopic: %s\nmessage:%s", __FILE__, __LINE__, __FUNCTION__, msg->topic_name, (char*)msg->message->payload);
    KAWAII_MQTT_LOG_I("----------------------\n");
    json=cJSON_Parse((char*)msg->message->payload); //获取整个大的句柄
    /*下面就是可以重复使用cJSON_GetObjectItem来获取每个成员的值了*/
    cJSON* item = cJSON_GetObjectItem(json,"svalue1");
    cJSON* itemidx = cJSON_GetObjectItem(json,"idx");
    int svalue1 = atoi(item->valuestring);
    int intidx = itemidx->valueint;
    printf("idx:%d\n",intidx);
    printf("svalue1:%d\n",svalue1);
    if(intidx == 158)          //小车控制
    {
      switch(svalue1)
    {
      case 0:
      {
          car_go();
          printf("car go!\n");
          break;
      }
      case 10:
      {
          car_back();
          rt_thread_mdelay(600);
          car_stop();
          printf("car back!\n");
          break;
      }
      case 20:
      {
          car_left();
          rt_thread_mdelay(400);
          car_stop();
          printf("car left!\n");
          break;
      }
      case 30:
      {
          car_right();
          rt_thread_mdelay(400);
          car_stop();
          printf("car right!\n");
          break;
      }
      default:
      {
          car_stop();
          printf("car stop!!\n");
          break;
      }
    }
    }else if(intidx == 161)    //小车RGB灯控制
    {
        switch(svalue1)
            {
              case 10:
              {
                  car_red_led();
                  printf("car red led on!\n");
                  break;
              }
              case 20:
              {
                  car_green_led();
                  printf("car green led on!\n");
                  break;
              }
              case 30:
              {
                  car_blue_led();
                  printf("car blue led on!\n");
                  break;
              }
              case 40:
               {
                  car_led_on();
                  printf("car all led on!\n");
                  break;
               }
              default:
              {
                  car_led_off();
                  printf("car led off!!\n");
                  break;
              }
            }
    }else if(intidx == 162)         //小车车灯控制
    {
      switch(svalue1)
    {
      case 10:
      {
          rt_pin_write(CAR_LEFT_LED, PIN_HIGH);
          printf("left LED on!\n");
          break;
      }
      case 20:
      {
          rt_pin_write(CAR_RIGHT_LED, PIN_HIGH);
          printf("right LED on!\n");
          break;
      }
      case 30:
      {
          rt_pin_write(CAR_LEFT_LED, PIN_HIGH);
          rt_pin_write(CAR_RIGHT_LED, PIN_HIGH);
          printf("left and right LED on!\n");
          break;
      }
      default:
      {
          rt_pin_write(CAR_LEFT_LED, PIN_LOW);
          rt_pin_write(CAR_RIGHT_LED, PIN_LOW);
          printf("car all LED off!!\n");
          break;
      }
    }
  }
    cJSON_Delete(json);
}


static int mqtt_publish_handle1(mqtt_client_t *client)
{
    char buf[128];
    rt_memset(&buf[0], 0, sizeof(buf));
    //定义light光线的
    char light_buf[128];
    rt_memset(&light_buf[0], 0, sizeof(light_buf));
    /* Domoticz MQTT格式 {"command": "switchlight", "idx": 158, "switchcmd": "Set Level", "level": 10 }*/
    cJSON* cjson_test = NULL;
    char* str = NULL;
    cJSON* cjson_light = NULL;
    char* light_str = NULL;
    /* 创建一个JSON数据对象(链表头结点) */
    cjson_test = cJSON_CreateObject();
    cjson_light = cJSON_CreateObject();
    /* 从消息队列中接收消息 */
    if (rt_mq_recv(&mq, &buf[0], sizeof(buf), RT_WAITING_FOREVER)
            == RT_EOK)
    {
        /* 输出内容 */
        rt_kprintf("mqtt recv msg:%s\n", buf);
    }

    /* 从光线light消息队列中接收消息 */
        if (rt_mq_recv(&light_mq, &light_buf[0], sizeof(light_buf), RT_WAITING_FOREVER)
                == RT_EOK)
        {
            /* 输出内容 */
            rt_kprintf("mqtt recv light_msg:%s\n", light_buf);
        }

    /* 添加一条整数型或字符串类型的JSON数据(添加一个链表节点) Domoticz温湿度的上传就这三个节点就行*/
    cJSON_AddNumberToObject(cjson_test, "idx", 159);     //小车环境数据在Domiticz设备中的idx
    cJSON_AddNumberToObject(cjson_test, "nvalue", 0);
    cJSON_AddStringToObject(cjson_test, "svalue", buf);

    /* 打印JSON对象(整条链表)的所有数据 */
    str = cJSON_PrintUnformatted(cjson_test);
    KAWAII_MQTT_LOG_I(str);
    mqtt_message_t msg;
    memset(&msg, 0, sizeof(msg));
    msg.qos = QOS0;
    msg.payload = (void *)str;
    mqtt_publish(client, MQTT_PUBTOPIC, &msg);

    rt_thread_mdelay(500);
    /* 添加一条整数型或字符串类型的JSON数据(添加一个链表节点) Domoticz光线的上传就这两个节点就行*/
    cJSON_AddNumberToObject(cjson_light, "idx", 160);     //小车光照数据在Domiticz设备中的idx
    cJSON_AddStringToObject(cjson_light, "svalue", light_buf);
    light_str = cJSON_PrintUnformatted(cjson_light);
    KAWAII_MQTT_LOG_I(light_str);

    mqtt_message_t light_msg;
    memset(&light_msg, 0, sizeof(light_msg));
    light_msg.qos = QOS0;
    light_msg.payload = (void *)light_str;
    return mqtt_publish(client, MQTT_PUBTOPIC, &light_msg);
}

static int mqtt_publish_handle_pir_on(mqtt_client_t *client)
{

    /* Domoticz MQTT格式 {"command": "switchlight", "idx": 163, "switchcmd": "On" }*/
    cJSON* cjson_test = NULL;
    char* str = NULL;

    /* 创建一个JSON数据对象(链表头结点) */
    cjson_test = cJSON_CreateObject();

    /* 添加一条整数型或字符串类型的JSON数据(添加一个链表节点) Domoticz温湿度的上传就这三个节点就行*/
    cJSON_AddStringToObject(cjson_test, "command", "switchlight");
    cJSON_AddNumberToObject(cjson_test, "idx", 163);     //小车人体在Domiticz设备中的idx
    cJSON_AddStringToObject(cjson_test, "switchcmd", "On");

    /* 打印JSON对象(整条链表)的所有数据 */
    str = cJSON_PrintUnformatted(cjson_test);
    KAWAII_MQTT_LOG_I(str);
    mqtt_message_t msg;
    memset(&msg, 0, sizeof(msg));
    msg.qos = QOS0;
    msg.payload = (void *)str;
    return mqtt_publish(client, MQTT_PUBTOPIC, &msg);

}

static int mqtt_publish_handle_pir_off(mqtt_client_t *client)
{

    /* Domoticz MQTT格式 {"command": "switchlight", "idx": 163, "switchcmd": "Off" }*/
    cJSON* cjson_test = NULL;
    char* str = NULL;

    /* 创建一个JSON数据对象(链表头结点) */
    cjson_test = cJSON_CreateObject();

    /* 添加一条整数型或字符串类型的JSON数据(添加一个链表节点) Domoticz温湿度的上传就这三个节点就行*/
    cJSON_AddStringToObject(cjson_test, "command", "switchlight");
    cJSON_AddNumberToObject(cjson_test, "idx", 163);     //小车人体在Domiticz设备中的idx
    cJSON_AddStringToObject(cjson_test, "switchcmd", "Off");

    /* 打印JSON对象(整条链表)的所有数据 */
    str = cJSON_PrintUnformatted(cjson_test);
    KAWAII_MQTT_LOG_I(str);
    mqtt_message_t msg;
    memset(&msg, 0, sizeof(msg));
    msg.qos = QOS0;
    msg.payload = (void *)str;
    return mqtt_publish(client, MQTT_PUBTOPIC, &msg);

}

/* MQTT线程入口函数*/
static void mqtt_thread_entry(void *parameter)
{


            rt_pin_mode(MOTO_3_1_PIN, PIN_MODE_OUTPUT);
            rt_pin_mode(MOTO_3_2_PIN, PIN_MODE_OUTPUT);
            rt_pin_mode(MOTO_2_1_PIN, PIN_MODE_OUTPUT);
            rt_pin_mode(MOTO_2_2_PIN, PIN_MODE_OUTPUT);
            rt_pin_mode(CAR_RED_PIN, PIN_MODE_OUTPUT);
            rt_pin_mode(CAR_GREEN_PIN, PIN_MODE_OUTPUT);
            rt_pin_mode(CAR_BLUE_PIN, PIN_MODE_OUTPUT);

            car_led_off();

            rt_thread_delay(6000);

            mqtt_log_init();

            client = mqtt_lease();

            mqtt_set_host(client, MQTT_HOST);
            mqtt_set_port(client, MQTT_PORT);
            mqtt_set_user_name(client, MQTT_USERNAME);
            mqtt_set_password(client, MQTT_PASSWORD);
            mqtt_set_client_id(client, MQTT_CLIENT);
            mqtt_set_clean_session(client, 1);

            rt_thread_delay(11000);  //Wifi的连接需要时间，这里延时10秒
            mqtt_connect(client);

            mqtt_subscribe(client, MQTT_SUBTOPIC, QOS0, sub_topic_handle1);

    while (1) {
        mqtt_publish_handle1(client);

        mqtt_sleep_ms(300 * 1000);
    }
}


/* MQTT Pir_on线程入口函数*/
static void mqtt_pir_on_entry(void *parameter)
{
    //mqtt_client_t *client = NULL;
    static rt_err_t result;
    //rt_thread_delay(6000);

    //mqtt_log_init();

    //client = mqtt_lease();

    //mqtt_set_host(client, MQTT_HOST);
    //mqtt_set_port(client, MQTT_PORT);
    //mqtt_set_user_name(client, MQTT_USERNAME);
    //mqtt_set_password(client, MQTT_PASSWORD);
    //mqtt_set_client_id(client, MQTT_CLIENT);
    //mqtt_set_clean_session(client, 1);

    rt_thread_delay(20000);  //Wifi的连接需要时间，这里延时20秒
    //mqtt_connect(client);

    //mqtt_subscribe(client, MQTT_SUBTOPIC, QOS0, sub_topic_handle1);

    while (1) {
        /* 永久方式等待信号量，获取到信号量，则执行MQTT上报 */
        result = rt_sem_take(pir_on_sem, RT_WAITING_FOREVER);
        if (result != RT_EOK)
                {
                    rt_kprintf("thread pir_on take a semaphore failed.\n");
                    rt_sem_delete(pir_on_sem);
                    return;
                }
                else
                {
                    mqtt_publish_handle_pir_on(client);

                    rt_thread_mdelay(500);
                    rt_pin_write(CAR_LEFT_LED, PIN_HIGH);
                    rt_pin_write(CAR_RIGHT_LED, PIN_HIGH);
                    car_blue_led();
                    rt_thread_mdelay(500);
                    rt_pin_write(CAR_LEFT_LED, PIN_LOW);
                    rt_pin_write(CAR_RIGHT_LED, PIN_LOW);
                    rt_thread_mdelay(500);
                    rt_pin_write(CAR_LEFT_LED, PIN_HIGH);
                    rt_pin_write(CAR_RIGHT_LED, PIN_HIGH);
                    rt_thread_mdelay(500);
                    rt_pin_write(CAR_LEFT_LED, PIN_LOW);
                    rt_pin_write(CAR_RIGHT_LED, PIN_LOW);   //有人左右车灯亮灭两次
                    //rt_thread_mdelay(28000);
                    //rt_sem_delete(pir_on_sem);

                }

    }
}

/* MQTT Pir_off线程入口函数*/
static void mqtt_pir_off_entry(void *parameter)
{
    static rt_err_t result;
    //rt_thread_delay(7000);

    //mqtt_log_init();

    //client = mqtt_lease();

    //mqtt_set_host(client, MQTT_HOST);
    //mqtt_set_port(client, MQTT_PORT);
    //mqtt_set_user_name(client, MQTT_USERNAME);
    //mqtt_set_password(client, MQTT_PASSWORD);
    //mqtt_set_client_id(client, MQTT_CLIENT);
    //mqtt_set_clean_session(client, 1);

    rt_thread_delay(20000);  //Wifi的连接需要时间，这里延时20秒
    //mqtt_connect(client);

    //mqtt_subscribe(client, MQTT_SUBTOPIC, QOS0, sub_topic_handle2);

    while (1) {
        /* 永久方式等待信号量，获取到信号量，则执行MQTT上报 */
        result = rt_sem_take(pir_off_sem, RT_WAITING_FOREVER);
        if (result != RT_EOK)
                {
                    rt_kprintf("thread pir_off take a semaphore failed.\n");
                    rt_sem_delete(pir_off_sem);
                    return;
                }
                else
                {
                    mqtt_publish_handle_pir_off(client);

                    car_led_off();
                    //rt_sem_delete(pir_off_sem);
                }

    }
}

static int app_mqtt_init(void)
{
    rt_err_t rt_err;
    /* 创建MQTT线程*/
    mqtt_thread = rt_thread_create("mqtt thread",     /* 线程的名称 */
                                    mqtt_thread_entry, /* 线程入口函数 */
                                    RT_NULL,            /* 线程入口函数的参数   */
                                    2048,                /* 线程栈大小，单位是字节  */
                                    23,                 /* 线程的优先级，数值越小优先级越高*/
                                    20);                /* 线程的时间片大小 */
    /* 如果获得线程控制块，启动这个线程 */
    if (mqtt_thread != RT_NULL)
        rt_err = rt_thread_startup(mqtt_thread);
    else
        rt_kprintf("mqtt thread create failure !!! \n");

    /* 判断线程是否创建成功 */
    if( rt_err != RT_EOK)
        rt_kprintf("mqtt thread startup err. \n");

    /* 创建MQTT_pir_on线程*/
        mqtt_pir_on = rt_thread_create("mqtt pir_on thread",     /* 线程的名称 */
                                        mqtt_pir_on_entry, /* 线程入口函数 */
                                        RT_NULL,            /* 线程入口函数的参数   */
                                        2048,                /* 线程栈大小，单位是字节  */
                                        22,                 /* 线程的优先级，数值越小优先级越高*/
                                        20);                /* 线程的时间片大小 */
        /* 如果获得线程控制块，启动这个线程 */
        if (mqtt_pir_on != RT_NULL)
            rt_err = rt_thread_startup(mqtt_pir_on);
        else
            rt_kprintf("mqtt pir_on create failure !!! \n");

        /* 判断线程是否创建成功 */
        if( rt_err != RT_EOK)
            rt_kprintf("mqtt pir_on startup err. \n");

        /* 再创建MQTT_pir_off线程*/
            mqtt_pir_off = rt_thread_create("mqtt pir_off thread",     /* 线程的名称 */
                                            mqtt_pir_off_entry, /* 线程入口函数 */
                                            RT_NULL,            /* 线程入口函数的参数   */
                                            2048,                /* 线程栈大小，单位是字节  */
                                            22,                 /* 线程的优先级，数值越小优先级越高*/
                                            10);                /* 线程的时间片大小 */
            /* 如果获得线程控制块，启动这个线程 */
            if (mqtt_pir_off != RT_NULL)
                rt_err = rt_thread_startup(mqtt_pir_off);
            else
                rt_kprintf("mqtt pir_off create failure !!! \n");

            /* 判断线程是否创建成功 */
            if( rt_err != RT_EOK)
                rt_kprintf("mqtt pir_off startup err. \n");

    return RT_EOK;
}

INIT_APP_EXPORT(app_mqtt_init);
