/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-09-02     RT-Thread    first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include <stdio.h>
#include <board.h>
#include "drv_common.h"
extern void car_right(void);
extern void car_stop(void);


#define LED_PIN GET_PIN(I, 8)
#define PIR_PIN GET_PIN(A, 7)
#define CAR_LEFT_LED GET_PIN(G, 13)
#define CAR_RIGHT_LED GET_PIN(G, 14)

/* PIR 触发信号量的指针 */
rt_sem_t pir_on_sem  = RT_NULL;
/* PIR 停止信号量的指针 */
rt_sem_t pir_off_sem  = RT_NULL;
int pir_pir = 0;    //PIR触发标记
int pir_count = 0;  //PIR循环计数

//PIR中断调用
void pir_thread()
{

    pir_pir = 1;           //人体触发，标志位置1
    pir_count = 0;         //计数归0
    rt_sem_release(pir_on_sem); //释放PIR触发信号量
    rt_kprintf("some body!\n");
}

int main(void)
{
    /* 创建一个动态信号量，初始值是0 */
    pir_on_sem = rt_sem_create("pir_on", 0, RT_IPC_FLAG_FIFO);
    if (pir_on_sem == RT_NULL)
    {
        rt_kprintf("create pir_on semaphore failed.\n");
        return -1;
    }
    else
    {
        rt_kprintf("create done. pir_on semaphore value = 0.\n");
    }

            /* 创建一个动态信号量，初始值是0 */
    pir_off_sem = rt_sem_create("pir_off", 0, RT_IPC_FLAG_FIFO);
    if (pir_off_sem == RT_NULL)
    {
        rt_kprintf("create pir_off semaphore failed.\n");
        return -1;
    }
    else
    {
        rt_kprintf("create done. pir_off semaphore value = 0.\n");
    }
    rt_uint32_t count = 1;

    rt_pin_mode(LED_PIN, PIN_MODE_OUTPUT);
    rt_pin_mode(CAR_LEFT_LED, PIN_MODE_OUTPUT);
    rt_pin_mode(CAR_RIGHT_LED, PIN_MODE_OUTPUT);
    //设置PIR引脚为上拉输入模式
    rt_pin_mode(PIR_PIN, PIN_MODE_INPUT_PULLUP);
    //绑定PIR引脚中断回调函数
    rt_pin_attach_irq(PIR_PIN, PIN_IRQ_MODE_FALLING, (void*)pir_thread, RT_NULL);

    //使能PIR引脚中断
    rt_pin_irq_enable(PIR_PIN, PIN_IRQ_ENABLE);

    //car_right();
    //rt_thread_mdelay(1600);    //差不多一圈360
    //car_stop();
    rt_pin_write(CAR_LEFT_LED, PIN_HIGH);
    rt_thread_mdelay(200);
    rt_pin_write(CAR_LEFT_LED, PIN_LOW);
    rt_thread_mdelay(200);
    rt_pin_write(CAR_RIGHT_LED, PIN_HIGH);
    rt_thread_mdelay(200);
    rt_pin_write(CAR_RIGHT_LED, PIN_LOW);
    rt_thread_mdelay(200);
    rt_pin_write(CAR_LEFT_LED, PIN_HIGH);
    rt_thread_mdelay(200);
    rt_pin_write(CAR_LEFT_LED, PIN_LOW);
    rt_thread_mdelay(200);
    rt_pin_write(CAR_RIGHT_LED, PIN_HIGH);
    rt_thread_mdelay(200);
    rt_pin_write(CAR_RIGHT_LED, PIN_LOW);   //左右车灯轮流亮灭两次
    while(count++)
    {
        if (pir_pir == 1)
        {
            if (pir_count >= 30)
            {
                rt_sem_release(pir_off_sem); //计时1分钟无触发，释放PIR停止信号量
                pir_pir = 0;   //PIR标志置0
                rt_kprintf("no body!\n");

            }
            pir_count++;
        }
        rt_thread_mdelay(1000);
        rt_pin_write(LED_PIN, PIN_HIGH);
        rt_thread_mdelay(1000);
        rt_pin_write(LED_PIN, PIN_LOW);
    }
    return RT_EOK;
}

#include "stm32h7xx.h"
static int vtor_config(void)
{
    /* Vector Table Relocation in Internal QSPI_FLASH */
    SCB->VTOR = QSPI_BASE;
    return 0;
}
INIT_BOARD_EXPORT(vtor_config);


