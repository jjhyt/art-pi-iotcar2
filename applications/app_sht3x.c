#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include <string.h>
#include <sht3x.h>

/* 消息队列控制块 */
struct rt_messagequeue mq;
/* 消息队列中用到的放置消息的内存池 */
static char msg_pool[2048];
/* 定义一个温湿度采集线程句柄结构体指针 */
static rt_thread_t sht3x_thread = RT_NULL;

/* 温湿度采集线程入口函数*/
static void sht3x_thread_entry(void *parameter)
{
    int result;
    char buf[16],hum1[3],hum2[2],tem1[3],tem2[2];

    sht3x_device_t  sht3x_device;
    sht3x_device = sht3x_init("i2c3", 0x44);

    //初始化后,此处需要短暂的延时，否则会出现连续读取失败的问题
    rt_thread_mdelay(150);

    while (1)
    {
        if(RT_EOK == sht3x_read_singleshot(sht3x_device))
        {
            rt_kprintf("sht30 humidity   : %d.%d  ", (int)sht3x_device->humidity, (int)(sht3x_device->humidity * 10) % 10);
            rt_kprintf("temperature: %d.%d\n", (int)sht3x_device->temperature, (int)(sht3x_device->temperature * 10) % 10);
            itoa((int)sht3x_device->humidity,hum1,10);
            itoa((int)(sht3x_device->humidity * 10) % 10,hum2,10);
            itoa((int)sht3x_device->temperature,tem1,10);
            itoa((int)(sht3x_device->temperature * 10) % 10,tem2,10);
            memset(buf,0,sizeof(buf));
            strcat(buf,tem1);
            strcat(buf,".");
            strcat(buf,tem2);
            strcat(buf,";");
            strcat(buf,hum1);
            strcat(buf,".");
            strcat(buf,hum2);
            strcat(buf,";");
            strcat(buf,"0");
            /* 发送消息到消息队列中 */
           result = rt_mq_send(&mq, buf, sizeof(buf));
           rt_kprintf("sht3x send msg: %s\n", buf);
           if ( result == -RT_EFULL)
           {
                  /* 消息队列满， 延迟10s时间 */
                  rt_kprintf("message queue full, delay 10s\n");
                  rt_thread_delay(10000);
            }

        }
        else
        {
            sht3x_softreset(sht3x_device);
            rt_kprintf("read sht3x fail.\r\n");
            break;
        }
        rt_thread_mdelay(300000);
    }

}


static int app_sht3x_init(void)
{
    rt_err_t rt_err;
    /* 初始化消息队列 */
    rt_mq_init(&mq, "mqt",
                &msg_pool[0], /* 内存池指向msg_pool */
                128 - sizeof(void*), /* 每个消息的大小是 128 - void* */
                sizeof(msg_pool),  /* 内存池的大小是msg_pool的大小 */
                RT_IPC_FLAG_FIFO); /* 如果有多个线程等待，按照FIFO的方法分配消息 */
    /* 创建温湿度采集线程*/
    sht3x_thread = rt_thread_create("sht3x thread",     /* 线程的名称 */
                                    sht3x_thread_entry, /* 线程入口函数 */
                                    RT_NULL,            /* 线程入口函数的参数   */
                                    1024,                /* 线程栈大小，单位是字节  */
                                    24,                 /* 线程的优先级，数值越小优先级越高*/
                                    10);                /* 线程的时间片大小 */
    /* 如果获得线程控制块，启动这个线程 */
    if (sht3x_thread != RT_NULL)
        rt_err = rt_thread_startup(sht3x_thread);
    else
        rt_kprintf("sht3x thread create failure !!! \n");

    /* 判断线程是否创建成功 */
    if( rt_err != RT_EOK)
        rt_kprintf("sht3x thread startup err. \n");

    return RT_EOK;
}

INIT_APP_EXPORT(app_sht3x_init);
